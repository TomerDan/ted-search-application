my_name = "TomerDaniely-DEV"


#network
vpc_cidr_block    = "10.0.0.0/16"
subnet_cidr_block = ["10.0.0.0/24"]
avilzone          = ["us-east-1a"]


#ec2
ami                    = "ami-08c40ec9ead489470"
instance_type          = "t2.micro"
key_name               = "TDkey"
expiration_date        = "30-11-2022"
bootcamp = "16"
