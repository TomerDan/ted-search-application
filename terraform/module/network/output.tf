output "vpc_id" {
  value = aws_vpc.vpc.id
}
output "sub_ids" {
  value = aws_subnet.subnet-public.*.id
}
output "secure_id" {
  value = aws_security_group.tomerdanielysctf.id
}

