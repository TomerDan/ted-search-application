 # NETWORK ##############################################################################################
 #vpc
resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr_block
  tags= {
      Name     = "${var.my_name}Terrafrom"
      bootcamp = "${var.bootcamp}"
      Owner = "${var.Owner}" 
      expiration_date = "${var.expiration_date}" 
  }
}
#subnet
resource "aws_subnet" "subnet-public" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  map_public_ip_on_launch = "true" //it makes this a public subnet
  cidr_block              = "${var.subnet_cidr_block}"
  availability_zone       = "${var.avilzone}"
  tags= {
      Name = "${var.my_name}-subnet-public-${count.index}"
  }
}
# igw
resource "aws_internet_gateway" "igw" {
    vpc_id = "${aws_vpc.vpc.id}"
    tags= {
        Name = "${var.my_name}-igw"
    }
}

# rt
resource "aws_route_table" "rt" {
    vpc_id = "${aws_vpc.vpc.id}"
    
    route {
        //associated subnet can reach everywhere
        cidr_block = "0.0.0.0/0"
        //CRT uses this IGW to reach internet
        gateway_id = "${aws_internet_gateway.igw.id}"
    }
    
    tags= {
        Name = "${var.my_name}-rout_table"
    }
}
# sssociated rt
resource "aws_route_table_association" "route_table_association"{
    subnet_id      = "${aws_subnet.subnet-public.id}"
    route_table_id = "${aws_route_table.rt.id}"
}

##### SECURITY GROUP ##########################################################################################################################

#Create security group with firewall rules
resource "aws_security_group" "tomerdanielysctf" {
  name        = "${var.my_name}-SCTF"
  description = "security group"
  vpc_id      = "${aws_vpc.vpc.id}"
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound from  server
  egress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.my_name}-SCTF"
  }
}