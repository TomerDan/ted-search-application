##### EC2 ##########################################################################################################################

resource "aws_instance" "web" {
    ami = var.ami
    instance_type = var.instance_type
    # VPC  
    subnet_id = "${var.subnet_id}"
    # Security Group
    vpc_security_group_ids = var.vpc_security_group_ids
    # the Public SSH key
    key_name = var.key_name
    user_data = var.user_data
    tags= {
        Name     = "${var.my_name}-Terrafrom"
        bootcamp = "${var.bootcamp}"
        Owner = "${var.Owner}" 
        expiration_date = "${var.expiration_date}" 
    }
    root_block_device {
    delete_on_termination = true
    tags= {
        Name     = "${var.my_name}-Terrafrom"
        bootcamp = "${var.bootcamp}"
        Owner = "${var.Owner}" 
        expiration_date = "${var.expiration_date}" 
    }
    }
}

