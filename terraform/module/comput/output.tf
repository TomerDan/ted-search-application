variable "my_name" {}
variable "ami" {}
variable "instance_type" {}
variable "key_name" {}

variable "vpc_security_group_ids" {}
variable "subnet_id" {}
variable "user_data" {}
variable "Owner" {}
variable "bootcamp" {}
variable "expiration_date" {}
variable "security_groups" {}
variable "subnets" {}
variable "instances" {}